import { createRouter, createWebHashHistory } from "vue-router";

import DashboardLayout from "@/layout/DashboardLayout";
import AuthLayout from "@/layout/AuthLayout";

import Users from "@/views/Users";
import Employees from "@/views/Employees";

import Login from "../views/Login.vue";
import Company from "@/views/Company";
import { hasPermission } from "@/helpers/general";
import store from "@/store/index";
import CompanyDetails from "@/views/CompanyDetails";

const routes = [
  {
    path: "/",
    redirect: "/users",
    component: DashboardLayout,
    children: [
      {
        path: "/companies",
        name: "companies",
        components: { default: Company },
        meta: { requiresAuth: true },
      },
      {
        path: "/companies/profile",
        name: "companies-profile",
        components: { default: CompanyDetails },
        meta: { requiresAuth: true },
      },
      {
        path: "/",
        name: "users",
        components: { default: Users },
        meta: { requiresAuth: true },
        beforeEnter: (to, from, next) => {
          if (hasPermission(store.state.auth.user, "view user")) {
            next();
          } else {
            next("/companies/profile");
          }
        },
      },
      {
        path: "/employees",
        name: "employees",
        components: { default: Employees },
        meta: { requiresAuth: true },
      },
    ],
  },
  {
    path: "/",
    redirect: "login",
    component: AuthLayout,
    children: [
      {
        path: "/login",
        name: "login",
        components: { default: Login },
      },
    ],
  },
];

const router = createRouter({
  history: createWebHashHistory(),
  linkActiveClass: "active",
  routes,
});

router.beforeEach((to, from, next) => {
  if (to.matched.some((record) => record.meta.requiresAuth)) {
    if (localStorage.getItem("user") == null) {
      next({
        path: "/login",
        params: { nextUrl: to.fullPath },
      });
    } else {
      let user = JSON.parse(localStorage.getItem("user"));
      if (to.matched.some((record) => record.meta.is_admin)) {
        if (user.is_admin === 1) {
          next();
        } else {
          next({ name: "userboard" });
        }
      } else {
        next();
      }
    }
  } else if (to.matched.some((record) => record.meta.guest)) {
    if (localStorage.getItem("jwt") == null) {
      next();
    } else {
      next({ name: "userboard" });
    }
  } else {
    next();
  }
});

export default router;
