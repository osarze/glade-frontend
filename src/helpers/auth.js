import axios from "axios";
export function login(credentials) {
  return new Promise((res, rej) => {
    axios
      .post("/api/login", credentials)
      .then((response) => {
        res(response.data);
      })
      .catch((err) => {
        if (err.response.data.error) {
          rej(err.response.data.error);
        }
        rej("Invalid email or password");
      });
  });
}

export function getLocalUser() {
  const userStr = localStorage.getItem("user");

  if (!userStr) {
    return null;
  }

  return JSON.parse(userStr);
}
