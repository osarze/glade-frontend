export function hasPermission(user, permission) {
  console.log(user.permissions, "user");
  return (
    user.permissions &&
    user.permissions.find((userPermission) => userPermission === permission)
  );
}
