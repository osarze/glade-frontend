let objCodec = require("object-encode");
let ryteops = ")*myNewAWESOME-realjayMyMan254@%^&%";
import { getLocalUser } from "./helpers/auth";

let user = null;
if (getLocalUser() == null) {
  user = null;
} else {
  user = objCodec.decode_object(getLocalUser(), "base64", ryteops);
}

export default {
  state: {
    currentUser: user,
    loading: false,
    auth_error: null,
    isLoggedIn: !!user,
  },

  mutations: {
    login(state) {
      state.loading = true;
      state.auth_error = null;
    },
    loginSuccess(state, payload) {
      state.auth_error = null;
      state.isLoggedIn = true;
      state.loading = false;

      state.currentUser = Object.assign(
        {},
        payload.user,
        { token: payload.token },
        { time: payload.expires_in },
        { permissions: payload.role.permission }
      );

      if (payload.user) {
        localStorage.setItem(
          "user",
          JSON.stringify(
            objCodec.encode_object(state.currentUser, "base64", ryteops)
          )
        );
      }
    },
    loginFailed(state, payload) {
      state.loading = false;
      state.auth_error = payload.error;
    },
    logout(state) {
      localStorage.removeItem("user");
      state.isLoggedIn = false;
      state.currentUser = null;
    },
  },
  getters: {
    isLoading(state) {
      return state.isloading;
    },
    isLoggedIn(state) {
      return state.isLoggedIn;
    },
    currentUser(state) {
      return state.currentUser;
    },
    authError(state) {
      return state.auth_error;
    },
  },
  actions: {
    login(context) {
      context.commit("login");
    },
  },
};
