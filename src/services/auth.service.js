import axios from "axios";

class AuthService {
  login(user) {
    return axios
      .post("login", {
        email: user.email,
        password: user.password,
      })
      .then((response) => {
        if (response.data.data.access_token) {
          localStorage.setItem("user", JSON.stringify(response.data.data));
        }

        return response.data;
      });
  }

  logout() {
    localStorage.removeItem("user");
  }
}

export default new AuthService();
