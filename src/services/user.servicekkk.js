import axios from "axios";
import authHeader from "./auth-headerkkkk";

// const API_URL = "http://localhost:8080/api/test/";

class UserServicekkk {
  getPublicContent() {
    return axios.get("all");
  }

  getUserBoard() {
    return axios.get("user", { headers: authHeader() });
  }

  getModeratorBoard() {
    return axios.get("mod", { headers: authHeader() });
  }

  getAdminBoard() {
    return axios.get("admin", { headers: authHeader() });
  }
}

export default new UserServicekkk();
